'use strict'
var sql_to_js_types = [
    {
        "SQL": "varchar",
        "code": "String",
        "": ""
    },
    {
        "SQL": "bigint",
        "code": "Number",
        "": ""
    },
    {
        "SQL": "binary",
        "code": "Byte[]",
        "": ""
    },
    {
        "SQL": "bit",
        "code": "Boolean",
        "": ""
    },
    {
        "SQL": "char",
        "code": "Char",
        "": ""
    },
    {
        "SQL": "timestamp",
        "code": "Date",
        "": ""
    },
    {
        "SQL": "date",
        "code": "Date",
        "": ""
    },
    {
        "SQL": "decimal",
        "code": "Float64Array",
        "": ""
    },
    {
        "SQL": "blob",
        "code": "String",
        "": ""
    },
    {
        "SQL": "float",
        "code": "Float64Array",
        "": ""
    },
    {
        "SQL": "int",
        "code": "Number",
        "": ""
    },
    {
        "SQL": "text",
        "code": "String",
        "": ""
    },
    {
        "SQL": "time",
        "code": "Date",
        "": ""
    }
];
var fs = require('fs');

function createSchemaJS(connection, config){

    connection.query('SHOW TABLES', async function (err, result, fields) {
        var urlAPI = "http://localhost:4400/api";
        var angularService = '../Mini-QA-Manager/src/app/Service'; // set the angular Service folder, I prefer use the external dir of this proyect. 
        var angularModel = '../Mini-QA-Manager/src/app/models'; // set the angular Model folder, I prefer use the external dir of this proyect. 
    
        var dirModels = __dirname+'/models';
        var dirController = __dirname+'/controllers';
        if (err) throw err;
        let model_templade = fs.readFileSync(__dirname+'/templades/model.js');
        let model_templade_ang = fs.readFileSync(__dirname+'/templades/model_ang.txt');
        let controller_templade = fs.readFileSync(__dirname+'/templades/controller.js');
        let router_templade = fs.readFileSync(__dirname+'/templades/dataservice.js');
        let service_templade = fs.readFileSync(__dirname+'/templades/dataServiceBase.txt');

        // CREATE FOLDERS IF NOT EXIST
        if (!fs.existsSync(dirModels)){
            fs.mkdirSync(dirModels);
        }
        if (!fs.existsSync(dirController)){
            fs.mkdirSync(dirController);
        }
        if (!fs.existsSync(angularService)){
            fs.mkdirSync(angularService);
        }
        if (!fs.existsSync(angularModel)){
            fs.mkdirSync(angularModel);
        }
        // creator process
        for (let table of result) {
            let table_name = table['Tables_in_' + config.database];
            //CREATE MODELS
            connection.query('DESCRIBE '+ table_name,  (err, cols, fields) => {
                if (err) throw err;
                let _v = '{';
                let _v_ang = '';
                for(let i= 0; i < cols.length; i++){
                    _v +='\''+[cols[i]['Field']]+'\': '+getType(cols[i]['Type'].split(' ')[0])+''+(i == cols.length-1?'}':',\n');
                    _v_ang +=[cols[i]['Field']]+': '+getType(cols[i]['Type'].split(' ')[0])+''+(i == cols.length-1?'\n':',\n');
                }
                // BACK END
                let tmp_model = paramsString(model_templade.toString(), [table_name, _v]);
                fs.writeFile(__dirname+'/models/'+table_name+'.js', tmp_model, (err) => {
                    // throws an error, you could also catch it here
                    if (err) throw err;
                    // success case, the file was saved
                    // console.log('Model '+table['Tables_in_dama_db']+' is ready!');
                });
                // FONT END
                model_templade_ang = model_templade_ang.toString().replace("'", "");
                let tmp_model_ang = paramsString(model_templade_ang, [table_name, _v_ang.replace('{', '').replace('}', '')]);
                fs.writeFile(angularModel+'/'+table_name+'.ts', tmp_model_ang, (err) => {
                    // throws an error, you could also catch it here
                    if (err) throw err;
                    // success case, the file was saved
                    // console.log('Model '+table['Tables_in_dama_db']+' is ready!');
                });
            });
            //CREATE CONTROLLERS
            let tmp_controller = paramsString(controller_templade.toString(), [table_name]);
            fs.writeFile(__dirname+'/controllers/'+table_name+'.js', tmp_controller, (err) => {
                // throws an error, you could also catch it here
                if (err) throw err;
            });
            // CREATE DATA SERVICE AND ROUTES
            let tmp_route = router_templade;
            tmp_route += "\n var "+table_name+" = require('../Controllers/"+table_name+"'); \n";

            service_templade = paramsString(service_templade.toString(), [urlAPI]);
            service_templade += "\n// -> "+table_name+"\n";
            // select 
            tmp_route += "router.post('/show/"+table_name+"', "+table_name+".showTable);\n";
            service_templade += "show_"+table_name+"(data): Observable<any> {\n"+
                "const params = JSON.stringify(data);\n"+
                "return this._http.post(this.url + '/show/"+table_name+"', params, {headers: this.headers});\n"+
              "}\n";
            // insert 
            tmp_route += "router.post('/insert/"+table_name+"', "+table_name+".insertInto);\n";  
            service_templade += "insert_"+table_name+"(data): Observable<any> {\n"+
                "const params = JSON.stringify(data);\n"+
                "return this._http.post(this.url + '/insert/"+table_name+"', params, {headers: this.headers});\n"+
              "}\n";
            // delete 
            tmp_route += "router.post('/delete/"+table_name+"', "+table_name+".deleteFrom);\n";  
            service_templade += "delete_"+table_name+"(data): Observable<any> {\n"+
            "const params = JSON.stringify(data);\n"+
            "return this._http.post(this.url + '/delete/"+table_name+"', params, {headers: this.headers});\n"+
          "}\n";
            // update 
            tmp_route += "router.post('/update/"+table_name+"', "+table_name+".update);\n";
            service_templade += "update_"+table_name+"(data): Observable<any> {\n"+
            "const params = JSON.stringify(data);\n"+
            "return this._http.post(this.url + '/update/"+table_name+"', params, {headers: this.headers});\n"+
          "}\n";
            // export route
            tmp_route += "module.exports = router;";  
            fs.writeFile(__dirname+'/routes/'+table_name+'.js', tmp_route, (err) => {
                // throws an error, you could also catch it here
                if (err) throw err;
            });
           
        }
        // CREATE SERVICE
        service_templade += "}";
        fs.writeFile(angularService+'/dataServiceBase.ts', service_templade, (err) => {
            // throws an error, you could also catch it here
            if (err) throw err;
        });
    });
}
// get the equals value in javascript format
function getType(typeSLQ) {
    var def = 'object';
    for (var ty of sql_to_js_types){
        if(typeSLQ.toLowerCase().includes(ty['SQL'].toLowerCase())){
            def = ty['code'];
            break;
        }
    }
    return def;
}
// remplace the text "%s" mark to some parameters in a string
function paramsString(text, paramps = []){
    var tmp_text = text;
    for (var i= 0; i < paramps.length; i++){
        tmp_text = tmp_text.split('%s'+(i+1)).join(paramps[i].toString());
    }
    return tmp_text;
}
module.exports = {
createSchemaJS
};
