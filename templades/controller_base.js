'use strict'
var checkingData= require('../escapeJSON');
/* var model= require('./../models/%s1');// get the model */
var table_name = '%s1';
// SELECT *FROM
// SELECT *FROM PLEASE BEFORE USE THE 'SELECT' CONDITION ESCAPE THE CONDITION
function showTable(contition) {
    return 'SELECT *FROM '+table_name+ ' '+ (contition!=undefined?' WHERE '+contition:'');
}
// INSERT INTO
function insertInto(values) {
    values = checkingData.checkJSON(values);
    var headers = Object.keys(values); 
    var _values = '';
    for (let i = 0; i < headers.length; i++){
        _values += values[headers[i]];
        _values += (i+1) == headers.length? '': ', ';
    }
    var query = 'INSERT INTO '+table_name+'('+headers.join(',')+') VALUES('+_values+')';
}
// DELETE
function deleteFrom(values, conditionType) {
    values = checkingData.checkJSON(values);
    conditionType = checkingData.checkJSON(conditionType);
    if (!Array.isArray(conditionType) || conditionType.length == 0 || conditionType == undefined) {
        throw "the conditions need to be an Array type"
    };
    var headers = Object.keys(values);
    var _condition_query = '';
    for (let i = 0; i < headers.length; i++){
        // if the array have conditions less than the values lenght, this use the first 
        const condition =i < conditionType.length?conditionType[i]: conditionType[0];
        if (values[headers[i]] != null || !values[headers[i]] != undefined){
            _condition_query += headers[i] +' = '+values[headers[i]];
            _condition_query += (i+1) == headers.length? '': ' AND ';
        }
    }
    var query = 'DELETE FROM '+table_name+' WHERE '+_condition_query;

}
// UPDATE
function update(values, conditions) {
    values = checkingData.checkJSON(values);
    conditions = checkingData.checkJSON(conditions);
    var headers = Object.keys(values);
    var headers_cond = Object.keys(conditions);
    var _condition_query = '';
    var _values_query = '';
    for (let i = 0; i < headers_cond.length; i++){
        _condition_query += headers_cond[i] +' = '+conditions[headers_cond[i]];
        _condition_query += (i+1) == headers_cond.length? '': ' AND ';
    }
    for (let i = 0; i < headers.length; i++){
        // if the array have conditions less than the values lenght, this use the first 
        if (values[headers[i]] != null || !values[headers[i]] != undefined){
            _values_query += headers[i] +' = '+values[headers[i]];
            _values_query += (i+1) == headers.length? '': ', ';
        }
    }
    var query = 'UPDATE '+table_name+' SET '+_values_query+' WHERE '+_condition_query;
    return query;
/*     app.connection.query(query, (err, result, fields) =>{
             if(err) { return {status: 0, message: err, 'result': result}}
            return {status: 1, message: 'query was executed correctly!', 'result': result}
        }); */
}
module.exports = {
showTable,
insertInto,
deleteFrom,
update
};