#About this project
This projected was focused to make an API manager for a QA project, but finally, we got an API generator for mysql. 
This project is currently in developing and we need to add more features.

### How to use it
Go to the app.js file in the main folder and change the settings according to your database profile.
```javascript
var connect_config = {
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'database_name'
};
```
after that run twice the project with the npm command:
 ` npm start`