'use strict' // get new features of nodejs
var app= require('./app');// get config of express server in app.js 
var db_schema= require('./db_schema_manager');// get config of express server in app.js 
var port = 4400; // set the port
app.connection.query('SELECT 1 + 1 AS solution', function(err, rows, fields) {
    if (err) throw err;
    app.mainApp.listen(port, () => {
        /** IMPORTANT 
         * YOU NEED TO RUN TWO TIMES THIS POJECT
         * FIRST TIME: CRETE THE COMPONENTS
         * SECOND TIME: CORRECT RUNNING OF THE SERVER
        */
        //create models and controllers if not exist
        db_schema.createSchemaJS(app.connection, app.Mysql_config);
        console.log("Server listening on: http://localhost:%s", port);
    });
  });