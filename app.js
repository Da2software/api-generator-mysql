'use strict'
const fs = require('fs');
var mysql = require("mysql"); // get mysql libary
var express = require('express'),
    cors = require('cors'),
    mainApp = express();//libreria que nos servira para yrabajar con rutas
var bodyParser = require('body-parser'); //libreria que nos servira  para convertir lo que biene en los post a objetos js

var connect_config = {
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'dama_db'
};


// Database connection
var connection = mysql.createConnection(connect_config);
connection.connect();


//Load routers
//var Queries_routers = require('./Routes/Queries');
//var Writers_routers = require('./Routes/Writers');

//middleware = metodo que se ejecuta antes que llegue a un conrolador
	//configuracion del bodyParser para convertir lo que biene del post a un objeto json
mainApp.use(bodyParser.urlencoded({extended : false }));
mainApp.use(bodyParser.json());

//cors
mainApp.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//rutas , requiere y respuesta
fs.readdir('./routes', (err, files) => {
    files.forEach(file => {
        let rt = require('./routes/'+file);
        mainApp.use('/api', rt);
   });
});

//exportar 
module.exports = {'mainApp': mainApp, 'connection': connection, 'Mysql_config': connect_config};
