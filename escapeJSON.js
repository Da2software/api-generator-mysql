'use strict'
var app = require('./app');
function checkJSON (json_values){
    try {
      var headers = Object.keys(json_values);
    var json_result = {};
    for (let header of headers) {
        json_result[header] = app.connection.escape(json_values[header]);
    }
    return json_result;  
    } catch(e) {
        return undefined;
    }
    
}
function checkArray (array_values){
    try {
        var array_result = [];
        for (_array of array_values) {
            array_result.push( app.connection.escape(_array));
        }
        return array_result; 
    } catch(e) {
        return undefined;
    }
 

}
module.exports = {
    checkArray,
    checkJSON
}